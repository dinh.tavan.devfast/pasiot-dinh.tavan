<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@pasiot.com',
            'password' => Hash::make('admin@pasiot.com'),
            'full_name' => 'Admin Pasiot',
            'phone' => '0123456789',
            'sex' => rand(User::USER_SEX_MAN, User::USER_SEX_WOMEN),
            'address' => 'HaNoi',
            'status' => rand(0, 1)
        ]);
        DB::table('users')->insert([
            'name' => 'User',
            'email' => 'user@pasiot.com',
            'password' => Hash::make('user@pasiot.com'),
            'full_name' => 'User Pasiot',
            'phone' => '0123456789',
            'sex' => rand(User::USER_SEX_MAN, User::USER_SEX_WOMEN),
            'address' => 'HaNoi',
            'status' => rand(0, 1)
        ]);
    }
}
