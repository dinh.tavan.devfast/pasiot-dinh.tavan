<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "full_name" => $this->full_name,
            'phone' => $this->phone,
            'address' => $this->address,
            'sex' => $this->sex,
            "status" => (string)$this->status,
            "password" => $this->password
        ];
    }
}
