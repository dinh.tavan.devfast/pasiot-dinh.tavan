<?php

namespace App\Services;

use App\Services\UserServiceInterface;
use App\Repositories\UserRepository;

class UserService implements UserServiceInterface
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get All Record
     * @return mixed
     */

    public function index()
    {
        return $this->userRepository->all();
    }

    /**
     * 
     * Get One Record
     * @return mixed
     */
    public function find($id)
    {
        return $this->userRepository->find($id);
    }

     /**
     * Create user
     * @param array
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->userRepository->create($data);
    }

    /**
     * Update
     * @param $id
     * @param array $data
     * @param array $options
     * @return mixed
     */
    public function update($id, array $data, array $options = [])
    {
        return $this->userRepository->updateById($id, $data, $options);
    }


    /**
     * Delete the specified model record from the database.
     *
     * @param $id
     */
    public function delete($id)
    {
        return $this->userRepository->deleteById($id);
    }

    /**
     * Search user
     *
     * @param $name
     */
    public function search($name)
    {
        return $this->userRepository->search($name);
    }
}
