<?php

namespace App\Repositories;

use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\UserRepositoryInterface;
use App\Models\User;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Search
     * @param $name
     * @param array $name
     * @return mixed
     */
    public function search($name){
        $result = $this -> model -> where('name', 'LIKE', '%'.$name.'%')->get();
        return $result;
    }

}
